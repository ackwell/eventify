#This is the modulous to make python look one folder above the current folder
import sys
sys.path.append("../")
import photoAPI, photoHTMLrenderer
import database_engine

def RenderSimpleuploader():
    return """
<form action='/pupload' method='post' enctype='multipart/form-data'>
  <input name='uploads' type=file>
  <br>Description:
  <input name='desc' type=input>
  <input type='submit'>
</form>
"""

def ProcessUpload(uploaddata, description):
    filename, filetype, data = uploaddata
    ident = photoAPI._photo_CreatePhotoEntry( 0, 0, description, filename)
    photoAPI._photo_CommitPicture(ident, data)
    return "Success!"

def ProcessUploadDP(uploaddata, userid):
    filename, filetype, data = uploaddata
    if filetype != "image/jpeg":
        print "IGNORED: Invalid uploaded filetype."
        return
    ident = photoAPI._photo_CreatePhotoEntry( userid, None, "", filename)
    photoAPI._photo_CommitPicture(ident, data)

def HandleEventMultiupload(uploaddatalist, userid, eventid):
    retstr = ""
    for uploaddata in uploaddatalist:
        filename, filetype, data = uploaddata
        if filetype != "image/jpeg":
            print "IGNORED: Invalid uploaded filetype."
            continue
        ident = photoAPI._photo_CreatePhotoEntry( userid, eventid, "", filename)
        photoAPI._photo_CommitPicture(ident, data)
        retstr += photoHTMLrenderer.imgtagRender( ident) + "<br><br>"
    return retstr

import sys
sys.path.append("../")

import database_engine
import hashlib

class User(object):
    def __init__(self, userid, username, firstname, lastname, email, password):
        self.userid = userid
        self.username = username
        self.firstname = firstname
        self.lastname = lastname
        self.email = email
        self.password = password

def hash_password(password):
    return hashlib.sha1(password).hexdigest()

def get_user(user):
    """
    Gets a User object.
    Returns None if username or userid is non-existant.
    """
    if user is None:
        return None # ...?
    conn = database_engine.Get_DatabaseConnection()
    curs = conn.cursor()
    if type(user) == int:
        curs.execute("""SELECT userid, username, firstname, lastname, email, password FROM users WHERE userid = ?""", (user,))
    elif isinstance(user, basestring): # It's ugly, but everything breaks otherwise
        curs.execute("""SELECT userid, username, firstname, lastname, email, password FROM users WHERE username = ?""", (user,))
    else:
        raise TypeError("user is not a string or an int")
    tup = curs.fetchone()
    if tup == None:
        return None
    return User(tup[0], tup[1], tup[2], tup[3], tup[4], tup[5])
    
def get_users(userids):
    """
    Gets a list of User objects from a list of userids.
    Returns None in list if userid is non-existant.
    """
    conn = database_engine.Get_DatabaseConnection()
    curs = conn.cursor()
    userlist = []
    for userid in userids:
        userlist.append(get_user(userid))
    return userlist

def get_user_from_email(email):
    conn = database_engine.Get_DatabaseConnection()
    curs = conn.cursor()
    if isinstance(email, basestring):
        curs.execute("""SELECT userid, username, firstname, lastname, email, password FROM users WHERE email = ?""", (email,))
    else:
        raise TypeError("email is not a string")
    tup = curs.fetchone()
    if tup == None:
        return None                                       
    return User(tup[0], tup[1], tup[2], tup[3], tup[4], tup[5])

def edit_user(user, hash_pass=False):
    """
    Updates a user identified by a user object and gives it the values in user object.
    If you are updating a password, set the hash_pass parameter (second parameter) to True
    NOTE: This will not change usernames or userids (at the moment)
    """
    conn = database_engine.Get_DatabaseConnection()
    curs = conn.cursor()
    curs.execute("""UPDATE users SET firstname = ?, lastname = ?, email = ?, password = ? WHERE userid = ?;""",\
        (user.firstname, user.lastname, user.email, hash_password(user.password) if hash_pass else user.password, user.userid))

def create_user(username, firstname, lastname, email, password):
    """
    Creates a user with assigned username, firstname, lastname, email and password.
    Returns the new User
    """
    conn = database_engine.Get_DatabaseConnection()
    curs = conn.cursor()
    # UID is auto incremented

    if get_user(username):
        return None
    if get_user_from_email(email):
        return None
    
    curs.execute("""INSERT INTO users(username, firstname, lastname, email, password) values (?, ?, ?, ?, ?)""",\
        (username, firstname, lastname, email, hash_password(password)))
    conn.commit()
    return get_user(username)

def delete_user(user):
    """
    This function is supposed to delete the given user (username or uid)
    But it raises a NotImplementedError!!!
    TROLOLOLOLOLOLOLOLOLOLO
    """
    raise NotImplemented("If you're using this at the moment, fuck you")
    conn = database_engine.Get_DatabaseConnection()
    curs = conn.cursor()
    if type(user) == int:
        curs.execute("""UPDATE users SET """ + k + """ = ? WHERE userid = ?;""", (data_dict[k], user))
    elif isinstance(user, basestring):
        pass

def make_friends(user, friend):
    """
    Makes the second user 'friend' a friend of the first user 'user'.
    Accepts a user object, user string, username
    """
    conn = database_engine.Get_DatabaseConnection()
    curs = conn.cursor()
    if type(user) == User:
        user = user.userid
    elif isinstance(user, basestring):
        user = get_user(user).userid
    if type(friend) == User:
        friend = friend.userid
    elif isinstance(friend, basestring):
        friend = get_user(friend).userid
    if type(user) == int:
        if type(friend) == int:
            curs.execute("""INSERT INTO friends(userid, friendid) values (?, ?)""", (user, friend))
        else:
            raise TypeError("%s friend is not a valid user object, uid or username string" % str(type(friend)))
    else:
        if type(friend) == int:
            raise TypeError("user is not a valid user object, uid or username string")
        else:
            raise TypeError("both user and friend are not valid user objects, uids or username strings")
    

def get_user_friends(user):
    """
    Gets the friends of a given user object, uid or username; as a tuple of userids
    """
    conn = database_engine.Get_DatabaseConnection()
    curs = conn.cursor()
    if type(user) == User:
        user = user.userid
    if type(user) == int:
        curs.execute("""SELECT friendid FROM friends WHERE userid = ?""", (user,))
    elif isinstance(user, basestring):
        curs.execute("""SELECT friendid FROM friends f JOIN users u ON u.userid = f.userid WHERE u.username = ?""", (user,))
    else:
        raise TypeError("user is not a valid user object, uid or username string")
    return [x[0] for x in curs.fetchall()]

def login(username, password):
    """
    Checks if a username, password pair match.
    If they do returns True, if not returns False.
    """
    conn = database_engine.Get_DatabaseConnection()
    curs = conn.cursor()
    curs.execute("""SELECT userid FROM users WHERE username = ? AND password = ?""", (username, hash_password(password)))
    if curs.fetchone():
        return True
    else:
        return False

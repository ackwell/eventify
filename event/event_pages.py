
import event


def page_init(server):
    server.register("/event/([0-9]+)" , event.page)
    server.register("/eventcreate", event.create)
    server.register("/attend" , event.ajax_attend)
    server.register("/eventlist" , event.eventlist)
    server.register("/editevent/([0-9]+)", event.edit_event)


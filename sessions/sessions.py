import random
import sqlite3
import ast
import sys
sys.path.append("../")
import database_engine

def delete_session(sessionid, response):
	'''Deletes a session with a given sessionid'''
	connection = database_engine.Get_DatabaseConnection()
	connection.execute('''DELETE FROM sessions WHERE sessionid=?''', (sessionid,))
	connection.commit()
	response.clear_cookie('sessionid')

def login(response, uid):
	'''Creates a session with a given uid'''
	logout(response)
	sessionid = get_session_id(response)
	save_session_uid(sessionid, uid)

def logout(response):
	sessionid = response.get_secure_cookie('sessionid')
	if sessionid is not None:
		delete_session(int(sessionid), response)

def has_session(response):
	sessionid = response.get_secure_cookie('sessionid')
	if sessionid is None:
		return False
	else:
		return True

def get_session_id(response):
	'''Returns session id given a response object. Makes a new session if needed'''
	sessionid = response.get_secure_cookie('sessionid')
	print "Sessionid is:", sessionid
	if sessionid is None:
		sessionid = _generate_session_id()
		connection = database_engine.Get_DatabaseConnection()
		connection.execute('''INSERT into sessions values(?, ?);''', (sessionid, None))
		connection.commit()
		response.set_secure_cookie('sessionid', str(sessionid))
	return int(sessionid)

def get_uid(response):
	''' Returns a user id given a response object'''
	if has_session(response):
		return get_session_uid(get_session_id(response))
	else:
		return None

def get_session_uid(sessionid):
	'''Returns a user id given a session id'''
	connection = database_engine.Get_DatabaseConnection()
	cur = connection.execute('''SELECT uid FROM sessions WHERE sessionid=?;''', (sessionid,))
	result = cur.fetchone()
	connection.commit()
	if result is not None:
		return int(result[0])
	return None

def save_session_uid(sessionid, uid):
	'''Updates a session's uid'''
	connection = database_engine.Get_DatabaseConnection()
	connection.execute('''UPDATE sessions SET uid=? WHERE sessionid=?;''', (uid, sessionid))
	connection.commit()

def _generate_session_id():
	connection = database_engine.Get_DatabaseConnection()
	cur = connection.execute('''SELECT max(sessionid) from sessions;''')
	biggest = cur.fetchone()[0]
	if biggest is None:
		biggest = 0
	sessionid = (biggest / 10**6 + 1) * 10**6 + random.randint(0, 10**6-1)
	return sessionid

def test(response):
	response.write('''
	<html>
	<body>
	<h1>Session test</h1>
	''')
	uid = get_uid(response)
	print 'Uid:', uid
	if uid is None:
		uid = random.randint(1, 100)
		login(response, uid)
	# response.write('<div>Session ID: ' + str(get_session_id(response)) + '</div>')
	response.write('<div>User ID: ' + str(uid) + '</div>')
	response.write('''
	</body>
	</html>''')
	



class FakeResponse(object):
	def __init__(self, cookie):
		self.cookie = cookie
	
	def get_secure_cookie(self, s):
		return self.cookie

	def set_secure_cookie(self, s, val):
		self.cookie = val
	
	def clear_cookie(self, s):
		self.cookie = None
	
	def write(self, s):
		print s



import sys
sys.path.append("../")
from user import usertools
import database_engine
class Event(object):
    def __init__(self, eventid, eventname, description, date, venue, hostid, attendees):
        self.eventid = eventid
        self.name = eventname
        self.description = description
        date = date.replace("/","-")
        self.date = date
        self.venue = venue
        self.hostid = hostid
        self.attendees = attendees
        
def get_event(eventid):
    """gets an event, returns a row from database"""
    conn = database_engine.Get_DatabaseConnection()
    curs = conn.cursor()
    curs.execute("""SELECT * FROM events WHERE eventid = ?;""", (eventid,))
    retval = curs.fetchone()
    if retval == None:
        return None
    event = Event(retval[0], retval[1], retval[2], retval[3], retval[4], retval[5], get_attendees(int(retval[0])))
    return event

def get_user_events(userid):
    """gets all events for a user ordered by time"""
    conn = database_engine.Get_DatabaseConnection()
    curs = conn.cursor()
    curs.execute("""SELECT e.* FROM events e INNER JOIN attendees a ON e.eventid = a.eventid WHERE a.userid = ? ORDER BY e.date;""", (userid,))
    retval = curs.fetchall()
    if retval == None:
        return None
    out = []
    for x in retval:
        out.append(Event(x[0], x[1], x[2], x[3], x[4], x[5], get_attendees(int(x[0]))))
    
    return out

def get_events():
    """gets all events"""
    conn = database_engine.Get_DatabaseConnection()
    curs = conn.cursor()
    curs.execute("""SELECT * FROM events""")
    retval = curs.fetchall()
    if retval == None:
        return None
    out = []
    for x in retval:
        out.append(Event(x[0], x[1], x[2], x[3], x[4], x[5], get_attendees(int(x[0]))))
    
    return out

def _getall_eventids():
    conn = database_engine.Get_DatabaseConnection()
    curs = conn.cursor()
    curs.execute("""SELECT eventid FROM events """)
    out = []
    for x in curs.fetchall():
        out.append(x[0])
    return out

def create_event(eventname, description, date, venue, hostid):
    """creates an event, returns its id"""
    conn = database_engine.Get_DatabaseConnection()
    curs = conn.cursor()
    date = date.replace("/","-")
    curs.execute("""INSERT INTO events(eventname, description, date, venue, hostid)
                values(?, ?, ?, ?, ?); """, (eventname, description, date, venue, hostid))
    conn.commit()
    return curs.lastrowid

def edit_event(eventid, eventname, description, date, venue, hostid):
    """updates event info"""
    conn = database_engine.Get_DatabaseConnection()
    curs = conn.cursor()
    date = date.replace("/","-")
    curs.execute("""UPDATE events SET eventname = ?, description = ?, date = ?, venue = ?, hostid = ? WHERE eventid = ?;""", (eventname, description, date, venue, hostid, eventid))
    conn.commit()
    
def attend_event(userid, eventid):
    """allows user to attend event"""
    conn = database_engine.Get_DatabaseConnection()
    curs = conn.cursor()
    curs.execute("""INSERT INTO attendees(userid, eventid) values (?, ?); """, (userid, eventid))
    conn.commit()

def unattend_event(userid, eventid):
    """allows user to attend event"""
    conn = database_engine.Get_DatabaseConnection()
    curs = conn.cursor()
    curs.execute("""DELETE FROM attendees WHERE userid = ? and eventid = ? ; """, (userid, eventid))
    conn.commit()

def get_attendees(eventid):
    """gets all attendees for event"""
    conn = database_engine.Get_DatabaseConnection()
    curs = conn.cursor()
    curs.execute("""SELECT userid FROM attendees WHERE eventid = ?;""", (eventid,))
    ids = []
    for row in curs.fetchall():
        ids.append(int(row[0]))    
    return usertools.get_users(ids)
    
def is_attending(userid , eventid):
    """checks if a user is attending an event"""
    conn = database_engine.Get_DatabaseConnection()
    curs = conn.cursor()
    curs.execute("""SELECT * FROM attendees WHERE eventid = ? and userid = ?;""", (eventid, userid))
    result = curs.fetchone()
    if result == None or len(result) == 0:  
        return False
    else:
        return True
    

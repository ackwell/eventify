import TemplateAPI
import sys
sys.path.append("../")
import eventtools
from sessions import sessions
from photos import photoHTMLrenderer, photoAPI
from datetime import date 


def page(response, eventid):
        event = eventtools.get_event(eventid)
        if event==None:
                response.redirect("/")
        else:
                uid = sessions.get_uid(response)
                context = {"event": event}
                if uid:
                        context["attending"] = eventtools.is_attending(uid , eventid)
                context["uid"] = uid
                photoidlist = photoAPI.ListPhotosOfEvent( eventid)
                gridoutput = photoHTMLrenderer.imggridRender( photoidlist, 4)
                context["gridhtml"] = gridoutput
                response.write(TemplateAPI.render("event.html", response, context))
        
def create(response):
        today = date.today()
        userid = sessions.get_uid(response)
        if userid == None:
                response.redirect("/login")
        title = response.get_field("title")
        if title == None:
                response.write(TemplateAPI.render("eventcreation.html", response, {"day":today.day, "month":today.month, "year":today.year}))
        else:
                #d = "%s/%s/%s"%(response.get_field("date_d"), response.get_field("date_m"), response.get_field("date_y"))
                d = response.get_field("date")
                
                eventid = eventtools.create_event(title , response.get_field("desc") , d, response.get_field("venue"), userid)
                response.redirect("/event/" + str(eventid))

def ajax_attend(response):
        status = response.get_field("attendstatus")
        eventid = response.get_field("eventid")
        userid = response.get_field("userid")
        if not any([status, eventid, userid]):
                response.redirect("/eventlist")
        status = int(status)
        eventid = int(eventid)
        userid = int(userid)
        if status == 1:
                eventtools.attend_event(userid, eventid)
        else:
                eventtools.unattend_event(userid, eventid)
        response.redirect("/event/" + str(eventid))
def eventlist(response):
        context = {"events" : eventtools.get_events()}
        response.write(TemplateAPI.render("eventlist.html", response , context))
        
def edit_event(response, eid):
        eventid = int(eid)
        userid = sessions.get_uid(response)
        if userid == None:
                response.redirect("/login")
        title = response.get_field("title")
        if title == None:
                response.write(TemplateAPI.render("editevent.html", response, {"eventid":eventid}))
        elif eventtools.get_event(eventid).hostid != userid:
                response.redirect("/event/" + str(eventid))
        else:
                eventtools.edit_event(eventid, title , response.get_field("desc") ,response.get_field("date") , response.get_field("venue"), userid)
                response.redirect("/event/" + str(eventid))
        

import sys
sys.path.append('../')
from sessions import sessions

def redirect(response):
    uid = sessions.get_uid(response)
    if uid != None:
        response.redirect('/profile/' + str(uid))
    else:
        response.redirect('/')

from user import usertools
from event import eventtools

# To avoid errors, delete the database before running this file

# To add a user use:
# usertools.create_user(username, firstname, lastname, email, password)

usertools.create_user('bob', 'Bob', 'Smith', 'bob@smith.com', 'bobpw')
usertools.create_user('nick', 'Nick', 'Armstrong', 'myemail@gmail.com', 'nickspassword')
usertools.create_user('dTawborn', 'Dim', 'Tawborn', 'dim@usyd.edu.au', 'pythonistehawesome')
usertools.create_user('JC', 'James', 'Curran', 'james.curran@usyd.edu.au', 'cinnamon')
usertools.create_user('yay', 'Ima', 'User', 'yay@teamtwo.com', 'Team2!')
usertools.create_user('bane','Rob', 'Un', 'rUn@zombies.com', 'brains')
#eventtools.create_event("BDO", "It's fun", "2012-12-03 00:00:00", "Park", 0)

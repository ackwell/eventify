#This is the modulous to make python look one folder above the current folder
import sys
sys.path.append("../")
import photoAPI
import database_engine

def GetImageURL( photoid):
    return photoAPI._photo_GetImageURL(photoid)

def imgtagRender( photoid):
    return "<img width=\"200\" height=\"200\" src=\"" + photoAPI._photo_GetImageURL(photoid) + "\">"

def imggridRender( photid_list, c=3):
    columns=3
    outstr = ""
    current_col = 0
    outstr += "<table width='70%'><tr>"
    for ID in photid_list:
        if current_col == columns:
            current_col = 0
            outstr += "</tr><tr>"

        current_col += 1
        outstr += "<td><a class='contImg' href=\"/picview?photo=" + str(ID) + "\">" + imgthumbRender(ID) + "</a></td>"

    outstr += "</tr></table>"
    return outstr
                
def imgthumbRender(photoid, width=150, height=150):
    return imgtagRender( photoid)

def PhotoPageRender(photoid, userid):
    imgloc = photoAPI._photo_GetImageURL(photoid)
    description = photoAPI.GetDescription( photoid)
    if photoAPI.GetEventID( photoid) == None or photoAPI.GetEventID( photoid) == "NULL":
        title = "Profile Picture"
    else:
        title = photoAPI.GetEventnameFromID(photoAPI.GetEventID(photoid))

    
    return imgloc, title, description, 0

def EditPhotoDataRender(photoid, userid):
    pass

def RenderEditButton(photoid):
    outstr = """
<button onClick="window.open('/picedit?photo=%s','Edit Picture','height=150,width=350');">Edit Tags and Description</button>
""" % str(photoid)
    return outstr

import sys
sys.path.append('../')

from user import usertools
import database_engine


class Result(object):
    def __init__(self, title, link, desc=""):
        self.title = title
        self.link = link
        self.desc = desc

def search_users(query):
    conn = database_engine.Get_DatabaseConnection()
    curs = conn.cursor()
    if not query:
        return []
    query = query.split()
    for t in xrange(len(query)):
        query[t] = '%' + query[t] + '%'
    results = []
    for t in query:
        curs.execute("""SELECT userid, username, firstname, lastname
                        FROM users
                        WHERE username
                        LIKE ? OR firstname LIKE ? OR lastname LIKE ?""", (t,t,t))
        for c in curs:
            for r in results:
                if r.title == c[1] + ': ' + c[2] + ' ' + c[3]:
                    break
            else:
                results.append(Result(c[1] + ': ' + c[2] + ' ' + c[3], "/profile/" + str(c[0])))
    return results

def search_photos(query):
    conn = database_engine.Get_DatabaseConnection()
    curs = conn.cursor()
    if not query:
        return []
    query = query.split()
    for t in xrange(len(query)):
        query[t] = '%' + query[t] + '%'
    results = []
    for t in query:
        curs.execute("""SELECT description, photoid
                        FROM photos 
                        WHERE description LIKE ?""", (t,))
        for c in curs:
            for r in results:
                if r.link == "/picview?photo=" + str(c[1]):
                    break
            else:
                results.append(Result(c[0],"/picview?photo=" + str(c[1]),""))
    for t in query:   
        curs.execute("""SELECT p.description, p.photoid
            FROM phototags pt
            JOIN photos p ON p.photoid = pt.photoid WHERE pt.tag LIKE ?""", (t,))
        for c in curs:
            for r in results:
                if r.link == "/picview?photo=" + str(c[1]):
                    break
            else:
                results.append(Result(c[0],"/picview?photo=" + str(c[1]),""))
    
    return results

def search_events(query):
    conn = database_engine.Get_DatabaseConnection()
    curs = conn.cursor()
    if not query:
        return []
    query = query.split()
    for t in xrange(len(query)):
        query[t] = '%' + query[t] + '%'
    results = []
    for t in query:
        curs.execute("""SELECT eventname, description, eventid
                        FROM events 
                        WHERE description LIKE ? OR eventname LIKE ?""", (t,t))
        for c in curs:
            for r in results:
                if r.title == c[0] and r.desc == c[1]:
                    break
            else:
                results.append(Result(c[0],"/event/" + str(c[2]),c[1]))
    return results

#This is the modulous to make python look one folder above the current folder
import sys
sys.path.append('../')

from user import usertools
from photos import photoAPI, photoHTMLrenderer
from event import eventtools
from sessions import sessions
import TemplateAPI

#--------------------------------------
#Called to return a list upcoming of events
def _profile_new_events(pageOwner):
    pass

#--------------------------------------
#Called to display the user's DP
def _profile_DP(pageOwner):
    DP = photoAPI.GetDP(pageOwner.userid)
    if DP == -1:
        return '/static/images/defDP.jpg'
    else:
        return photoAPI._photo_GetImageURL(DP)

#--------------------------------------
#Called to generate code for to render the images
#Uploded by the user who owns the page
#DO NOT CALL OUTSIDE OF THIS FILE!!!

def _profile_render_photos(pageOwner):   ##WORK ON THIS
    prHTML = ''
    p = photoAPI
    pr = photoHTMLrenderer
    photoList = photoAPI.GetPhotosCreatedByUser(pageOwner.userid)
    prHTML = photoHTMLrenderer.imggridRender(photoList)
    return prHTML

#--------------------------------------
#Make friends with the page owner
#DO NOT CALL OUTSIDE OF THIS FILE!!! (Unless it's really neeeded)
def _profile_make_friends(response):
    OtherID = response.get_field('userid')
    curUser = sessions.get_uid(response)
    curUser = usertools.get_user(int(curUser))
    otherUser = usertools.get_user(int(OtherID))
    usertools.make_friends(curUser.userid, otherUser.userid)
    print curUser.username + " is now friends with " + otherUser.username
    response.redirect("/profile")

#--------------------------------------    
#Renders the page
#DO NOT CALL OUTSIDE OF THIS FILE!!!

def _profile_render(response, curUser, pageOwner):
    #if usertools.get_user_friends(pageOwner.userid):
    friends = [usertools.get_user(x) for x in usertools.get_user_friends(pageOwner.userid)]
    events = eventtools.get_user_events(pageOwner.userid)
    #else:
    #    friends = []

    curfriends = usertools.get_user_friends(curUser.userid)
    prHTML = _profile_render_photos(pageOwner)
    DP = _profile_DP(pageOwner)
    #If the current user owns the current profile
    if curUser == None:
        response.redirect("/")
    #Else redirect to home
    else:
        response.write(TemplateAPI.render('profile.html', response, {'user': pageOwner, \
                                                                     'curfriends': curfriends,  'firstname' : pageOwner.firstname, 'friends' : friends, \
                                                       'isMyProfile': (curUser==pageOwner),'photos' : prHTML, 'DP' : DP, 'all_events': events}))
    
    

        
#--------------------------------------
#Called when the page is loaded.
#Assigns the page owner and the current user
def profile(response, uid):

    #Asigns a userID
    curSession = sessions.get_uid(response)
    if not curSession:
        response.redirect("/")
    curUser = usertools.get_user(int(curSession))
    pageOwner = usertools.get_user(int(uid))
    curUser = usertools.get_user(curSession)    
    _profile_render(response, curUser, pageOwner)

